package de.maddin.entities;

import de.gurkenlabs.litiengine.input.KeyboardEntityController;

import java.awt.event.KeyEvent;

public class PlayerController extends KeyboardEntityController<Player> {

    public PlayerController(Player entity) {
        super(entity);

        this.addUpKey(KeyEvent.VK_W);
        this.addDownKey(KeyEvent.VK_S);
        this.addLeftKey(KeyEvent.VK_A);
        this.addRightKey(KeyEvent.VK_D);
    }

    @Override
    public void handlePressedKey(KeyEvent keyCode) {
        super.handlePressedKey(keyCode);
    }
}
