package de.maddin.entities;

import de.gurkenlabs.litiengine.Align;
import de.gurkenlabs.litiengine.Valign;
import de.gurkenlabs.litiengine.entities.*;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.graphics.animation.CreatureAnimationController;
import de.gurkenlabs.litiengine.graphics.animation.IEntityAnimationController;
import de.gurkenlabs.litiengine.physics.IMovementController;

@EntityInfo(width = 11, height = 20)
@CollisionInfo(collision = true, collisionBoxWidth = 8, collisionBoxHeight = 8, align = Align.CENTER, valign = Valign.DOWN)
@MovementInfo(velocity = 100)
@AnimationInfo(spritePrefix = "player")
public class Player extends Creature {

    private static Player instance;

    private Player() {
        super("player");
    }

    public static Player instance() {

        if (instance == null) {
            instance = new Player();
        }
        return instance;
    }

    @Override
    public String getSpritesheetName() {
        return "player";
    }

    @Override
    protected IMovementController createMovementController() {
        return new PlayerController(this);
    }

    /*@Override
    protected IEntityAnimationController<?> createAnimationController() {
        IEntityAnimationController<?> controller = new CreatureAnimationController<>(this, false);
        //controller.add(new Animation("player-idle", true, true));
        //controller.addRule(x -> Player.instance().isIdle(), x -> "player-idle");
        return controller;
    }*/
}
