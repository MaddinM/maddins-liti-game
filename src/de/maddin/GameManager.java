package de.maddin;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.Spawnpoint;
import de.gurkenlabs.litiengine.environment.CreatureMapObjectLoader;
import de.gurkenlabs.litiengine.environment.Environment;
import de.gurkenlabs.litiengine.environment.EnvironmentListener;
import de.gurkenlabs.litiengine.graphics.Camera;
import de.gurkenlabs.litiengine.graphics.PositionLockCamera;
import de.gurkenlabs.litiengine.graphics.Spritesheet;
import de.gurkenlabs.litiengine.resources.Resources;
import de.maddin.entities.Player;

public final class GameManager {

    public static void init() {

        Camera camera = new PositionLockCamera(Player.instance());
        camera.setClampToMap(true);
        Game.world().setCamera(camera);

        CreatureMapObjectLoader.registerCustomCreatureType(Player.class);

        Game.world().addListener(new EnvironmentListener() {

            @Override
            public void initialized(Environment e) {
                Spawnpoint playerSpawnpoint = new Spawnpoint(400, 400);
                playerSpawnpoint.spawn(Player.instance());
            }

            @Override
            public void loaded(Environment e) {
                Game.audio().playMusic(Resources.sounds().get("background.ogg"));
            }
        });

        Game.loop().attach(GameManager::update);
    }

    private static void update() {

    }
}
