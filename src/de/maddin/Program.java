package de.maddin;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.environment.Environment;
import de.gurkenlabs.litiengine.graphics.Spritesheet;
import de.gurkenlabs.litiengine.resources.Resources;
import de.maddin.gui.IngameScreen;

import static de.gurkenlabs.litiengine.gui.screens.Resolution.Ratio16x9.RES_1280x720;

public class Program {

    public static void main(String[] args) {

        Game.info().setName("Maddin's Game");
        Game.info().setSubTitle("");
        Game.info().setVersion("0.0.1");
        Game.info().setWebsite("");
        Game.info().setDescription("");

        //TODO resource-util class
        Spritesheet playerIdleUp = Resources.spritesheets().load("player-idle-up.png", 16, 16);
        Resources.spritesheets().add("player-idle-up", playerIdleUp);
        Spritesheet playerIdleDown = Resources.spritesheets().load("player-idle-down.png", 16, 16);
        Resources.spritesheets().add("player-idle-down", playerIdleDown);
        Spritesheet playerIdleLeft = Resources.spritesheets().load("player-idle-left.png", 16, 16);
        Resources.spritesheets().add("player-idle-left", playerIdleLeft);
        Spritesheet playerIdleRight = Resources.spritesheets().load("player-idle-right.png", 16, 16);
        Resources.spritesheets().add("player-idle-right", playerIdleRight);

        Spritesheet playerWalkUp = Resources.spritesheets().load("player-walk-up.png", 16, 16);
        Resources.spritesheets().add("player-walk-up", playerWalkUp);
        Spritesheet playerWalkDown = Resources.spritesheets().load("player-walk-down.png", 16, 16);
        Resources.spritesheets().add("player-walk-down", playerWalkDown);
        Spritesheet playerWalkLeft = Resources.spritesheets().load("player-walk-left.png", 16, 16);
        Resources.spritesheets().add("player-walk-left", playerWalkLeft);
        Spritesheet playerWalkRight = Resources.spritesheets().load("player-walk-right.png", 16, 16);
        Resources.spritesheets().add("player-walk-right", playerWalkRight);

        Game.init(args);
        GameManager.init();

        Game.graphics().setBaseRenderScale(4.001f);
        Game.window().setResolution(RES_1280x720);

        Environment environment = new Environment(Resources.maps().get("test-map-big.tmx"));
        Game.world().loadEnvironment(environment);
        Game.screens().add(new IngameScreen());

        Game.start();
    }
}
